#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 08:48:15 2020

@author: tp
"""


import unittest
from main.model.Hero import Hero


class SimpleTestHero(unittest.TestCase):

    def test_hero_creation(self):
        h = Hero("test")
        self.assertTrue(h, None)

    def test_hero_name_init(self):
        h = Hero("test")
        self.assertEqual(h.get_name(), "test")


if __name__ == '__main__':
    unittest.main()

